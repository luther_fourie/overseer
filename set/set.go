package set

import (
	"sync"
)

type ConcurrentSet struct {
	m map[interface{}]struct{}
	l sync.RWMutex
}

func NewConcurrentSet() *ConcurrentSet {
	return &ConcurrentSet{
		m: make(map[interface{}]struct{}),
	}
}

func (s *ConcurrentSet) Add(e interface{}) {
	s.l.Lock()
	defer s.l.Unlock()

	s.m[e] = struct{}{}
}

func (s *ConcurrentSet) Remove(e interface{}) {
	s.l.Lock()
	defer s.l.Unlock()

	delete(s.m, e)
}

func (s *ConcurrentSet) Size() int {
	s.l.RLock()
	defer s.l.RUnlock()

	return len(s.m)
}

func (s *ConcurrentSet) Contains(e interface{}) (exists bool) {
	s.l.RLock()
	defer s.l.RUnlock()

	_, exists = s.m[e]
	return
}
