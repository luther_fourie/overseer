// watchtower project main.go
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	fsnotify "gopkg.in/fsnotify.v1"
	"io/ioutil"
	"log"
	"os/exec"
	"overseer/chanutil"
	"overseer/set"
	"path/filepath"
	"time"
	"wc"
)

var (
	config Config
)

// Overseer is a file watcher and external task runner for web development
type Overseer struct {
	wc.WC

	watcher       *fsnotify.Watcher
	watcherEvents chan fsnotify.Event
	fileChangeSet *set.ConcurrentSet

	webservice *Webservice
}

// Intializes overseer
func (o *Overseer) Init() {
	o.webservice = new(Webservice)
	o.Add(o.webservice)

	var err error
	o.watcher, err = fsnotify.NewWatcher()
	o.Check(err)

	o.fileChangeSet = set.NewConcurrentSet()

	o.watcherEvents = chanutil.Filter(o.watcher.Events, func(e interface{}) bool {
		event := e.(fsnotify.Event)

		if !((event.Op&fsnotify.Write == fsnotify.Write) ||
			(event.Op&fsnotify.Create == fsnotify.Create)) {
			return false
		}

		if o.fileChangeSet.Contains(e) {
			return false
		}

		o.fileChangeSet.Add(e)
		time.Sleep(100 * time.Millisecond)
		o.fileChangeSet.Remove(e)

		return true
	}).(chan fsnotify.Event)
}

// Starts overseer
func (o *Overseer) Start() {
	go func() {
		for {
			select {
			case event := <-o.watcherEvents:
				o.fileChanged(event)

			case err := <-o.watcher.Errors:
				log.Println("error:", err)
			}
		}
	}()

	/*
		go func() {
			i := 0
			for {
				i++
				time.Sleep(1 * time.Second)
				cmd := Commands().Print(fmt.Sprintf("Hi there, I am #%d", i))
				o.webservice.WriteCmdToListeners(cmd)
			}
		}()
	*/
	err := o.watcher.Add(`assets\styles\`)
	o.Check(err)
}

// Stop stops overseer
func (o *Overseer) Stop() {
	o.watcher.Close()
}

// fileChanged is called when a file has changed
func (o *Overseer) fileChanged(event fsnotify.Event) {
	name := filepath.ToSlash(event.Name)

	fmt.Println("File changed:", event.String())
	o.processStylesheets(name)
}

// watch  adds a new item to the wather
func (o *Overseer) watch(name string) {
	o.watcher.Add(name)
}

// processStylesheets does glob pattern matching on 'name' to check for any
// stylesheet changes that should occur
func (o *Overseer) processStylesheets(name string) {
	processed := false
	for k, v := range config.Monitor.Stylesheets {

		matched, err := filepath.Match(k, name)
		if err != nil {
			panic(err)
		}

		if matched {
			stdout, stderr, err := o.runCmd(v)
			if err != nil {
				msg := fmt.Sprintln("Could not write command '", v, " ':", err, "\nStdout:\n", stdout, "\nStderr:\n", stderr)
				o.webservice.WriteCmdToListeners(Commands().Print(msg))

				// do not continue
				return
			}

			fmt.Println("Command executed...", v)
			msg := fmt.Sprintln(v, " returned: ", stdout)
			o.webservice.WriteCmdToListeners(Commands().Print(msg))

			processed = true
		}
	}
	if processed {
		o.stylesheetsProcessed()
	} else {
		fmt.Println("Failed to match ", name)
	}
}

// runCmd runs the 'command' via cmd.exe
func (o *Overseer) runCmd(command string) (stdout *bytes.Buffer, stderr *bytes.Buffer, err error) {
	cmd := exec.Command("cmd", "/C", command)

	stdout = new(bytes.Buffer)
	cmd.Stdout = stdout

	stderr = new(bytes.Buffer)
	cmd.Stderr = stderr

	return stdout, stderr, cmd.Run()
}

// stylesheetsProcessed is called when processing has occured on stylesheets
func (o *Overseer) stylesheetsProcessed() {
	// notify all listeners
	o.webservice.WriteCmdToListeners(Commands().ReloadStylesheets())
}

// parseConfig parses the config specified by filename
func parseConfig(filename string) error {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(b, &config); err != nil {
		panic(err)
	}

	return nil
}
