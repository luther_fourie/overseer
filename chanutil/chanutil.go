package chanutil

import (
	"reflect"
)

func Filter(srcCh interface{}, fn func(e interface{}) bool) interface{} {
	srcCV := reflect.ValueOf(srcCh)
	dstCV := reflect.MakeChan(srcCV.Type(), 0)

	go func() {
		for {
			x, ok := srcCV.Recv()
			if !ok {
				dstCV.Close()
				return
			}

			go func() {
				if fn(x.Interface()) {
					dstCV.Send(x)
				}
			}()
		}
	}()

	return dstCV.Interface()
}
