package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"overseer/collection"
	//"sync"
	"sync/atomic"
	"wc"
)

// Webservice represents all web related logic of the overseer application
type Webservice struct {
	wc.WC

	// * config properties *
	ReadBufferSize  int
	WriteBufferSize int
	ListenAddress   string

	upgrader         websocket.Upgrader
	lastConnectionId int32
	connections      *collection.ConcurrentMap
}

// WsConn has a bit of a muckup, go conventions dictate that senders of channels
// close and readers check for closed, this will eradicate the need for that
// terrible close lock and helpers
type WsConn struct {
	id     int32
	conn   *websocket.Conn
	read   chan *WsCmd
	write  chan *WsCmd
	closer chan int

	closed int32
}

// NewWsConn creates a WsConn object
func NewWsConn(id int32, conn *websocket.Conn) *WsConn {
	return &WsConn{
		id:     id,
		conn:   conn,
		closed: 0,
		read:   make(chan *WsCmd),
		write:  make(chan *WsCmd),
		closer: make(chan int, 3),
	}
}

// handle handles read and write requests over the websocket connection, since
// concurrent read and write operations are not supported by the websocket
// connection, handle creates reader and writer routines waiting on channels
// for their requests
func (c *WsConn) handle() {
	// concurrent calls to read and write methods are not supported
	// reader and writer routines aim to overcome this

	// reader
	go func() {
		for {
			cmd := &WsCmd{}
			err := c.conn.ReadJSON(cmd)
			if err != nil {
				c.Close()
				close(c.read)
				return
			}

			select {
			case c.read <- cmd:
			case <-c.closer:
				{
					c.closer <- 0
					close(c.read)
					return
				}
			}
		}
	}()

	// writer
	go func() {
		for {
			select {
			case cmd, ok := <-c.write:
				if !ok {
					return
				}

				err := c.conn.WriteJSON(cmd)
				if err != nil {
					c.Close()
					return
				}

			case <-c.closer:
				{
					//Do not close channel as we are a reciever
					c.closer <- 0
					return
				}
			}
		}
	}()
}

// Close closes the websocet connection as well as all channels, close can
// safely be called multiple times
func (c *WsConn) Close() {
	if atomic.SwapInt32(&c.closed, 1) == 1 {
		return
	}
	c.closer <- 0
	c.closer <- 0

	if err := c.conn.Close(); err != nil {
		fmt.Println("WARN: Could not close connection:", err)
	}
}

// isClosed is a synchronized function for checking if the connection is closed
func (c *WsConn) isClosed() bool {
	return atomic.LoadInt32(&c.closed) == 1
}

// WriteCmd queues cmd for writing over the connection. An error is returned if
// the connection is closed. Blocks until command is queued
func (c *WsConn) WriteCmd(cmd *WsCmd) error {
	select {
	case c.write <- cmd:
	case <-c.closer:
		{
			c.closer <- 0
			return fmt.Errorf("the connection is closed")
		}
	}
	return nil
}

// Init initializes the webservice
func (ws *Webservice) Init() {
	// manually set config properties
	ws.ReadBufferSize = 1024
	ws.WriteBufferSize = 1024
	ws.ListenAddress = ":11235"

	ws.upgrader = websocket.Upgrader{
		ReadBufferSize:  ws.ReadBufferSize,
		WriteBufferSize: ws.WriteBufferSize,
	}
	ws.connections = collection.NewConcurrentMap()

	http.HandleFunc("/overseer", ws.websocketHandler)
	http.Handle("/", http.FileServer(http.Dir("dist/dev")))
}

// Start starts the webservice
func (ws *Webservice) Start() {
	err := http.ListenAndServe(ws.ListenAddress, nil)
	ws.Check(err)
}

// websocketHandler handles websocket connections
func (s *Webservice) websocketHandler(w http.ResponseWriter, r *http.Request) {
	wsConn, err := s.makeWsConn(w, r)
	s.Check(err)

	for {
		cmd, ok := <-wsConn.read
		if !ok {
			// connection is closed
			s.connections.Remove(wsConn.id)
			return
		}
		fmt.Println("Cmd read:", cmd)
	}
}

// makeWsConn takes the provided http request and response parameters and
// upgrades it to a websocket connection, the connection is then added to the
// connection list of the webservice
func (s *Webservice) makeWsConn(w http.ResponseWriter, r *http.Request) (*WsConn, error) {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		return nil, err
	}

	id := atomic.AddInt32(&s.lastConnectionId, 1)
	wsConn := NewWsConn(id, conn)
	s.connections.Put(id, wsConn)
	wsConn.handle()
	return wsConn, nil
}

// WriteCmdToListeners writes the specified command to all active non-closed
// websocket connections
func (s *Webservice) WriteCmdToListeners(cmd *WsCmd) {
	for c := range s.connections.Iter() {
		wsConn := c.Value.(*WsConn)
		err := wsConn.WriteCmd(cmd)
		if err != nil {
			fmt.Println(err)
		}
	}
}

// WsCmd represents an incoming or outgoing websocket command
type WsCmd struct {
	Name   string
	Params interface{}
}

// WsCommands provides a "namespace" for factory functions returning WsCmd
// objects
type WsCommands struct {
}

// Commands returns an empty WsCommand object
func Commands() *WsCommands {
	return (*WsCommands)(nil)
}

// ReloadStylesheets instructs the browser to attempt a live reload of all
// stylesheets
func (c *WsCommands) ReloadStylesheets() *WsCmd {
	return &WsCmd{"ReloadStylesheets", nil}
}

// Print instructs the browser to print a message to the overseer window
func (c *WsCommands) Print(msg string) *WsCmd {
	return &WsCmd{"Print", struct {
		Msg string
	}{msg}}
}
