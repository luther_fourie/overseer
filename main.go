package main

import (
	"os"
	"wc"
)

// todo: implement config file monitoring
type Config struct {
	Monitor Monitor
}

type Monitor struct {
	wc.WC

	Stylesheets map[string]string
	Markup      map[string]string
	Scripts     map[string]string
}

func main() {
	err := os.Chdir(`D:/Temp/community-mock-site2`)
	wc.Check(err)

	err = parseConfig("C:\\Go programs\\src\\overseer\\config.json")
	wc.Check(err)

	wc.Add(new(Overseer))
	wc.StartApp()
	wc.WaitForTerminate()
}
