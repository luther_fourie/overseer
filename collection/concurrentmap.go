package collection

import (
	"sync"
)

// ConcurrentMap provides functions for synchronized map access, values should
// be small/reference types
type ConcurrentMap struct {
	m map[interface{}]interface{}
	l sync.RWMutex
}

type Pair struct {
	Key   interface{}
	Value interface{}
}

func NewConcurrentMap() *ConcurrentMap {
	return &ConcurrentMap{
		m: make(map[interface{}]interface{}),
	}
}

func (cm *ConcurrentMap) Put(key interface{}, value interface{}) {
	cm.l.Lock()
	defer cm.l.Unlock()

	cm.m[key] = value
}

func (cm *ConcurrentMap) Remove(key interface{}) {
	cm.l.Lock()
	defer cm.l.Unlock()

	delete(cm.m, key)
}

func (cm *ConcurrentMap) Get(key interface{}) (value interface{}, exists bool) {
	cm.l.RLock()
	defer cm.l.RUnlock()

	value, exists = cm.m[key]
	return
}

func (cm *ConcurrentMap) GetPairs() (pairs []Pair) {
	cm.l.RLock()
	defer cm.l.RUnlock()

	for k, v := range cm.m {
		pairs = append(pairs, Pair{
			Key:   k,
			Value: v,
		})
	}

	return
}

func (cm *ConcurrentMap) Iter() (pairs chan Pair) {
	pairs = make(chan Pair)

	go func() {
		pairList := cm.GetPairs()
		for _, p := range pairList {
			pairs <- p
		}
		close(pairs)
	}()

	return
}
